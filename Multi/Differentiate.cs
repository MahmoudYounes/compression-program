﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompressionProject
{
    class Differentiate
    {
        public ImagePGM GenerateDifferentiatedImage(ImagePGM In)
        {
            ImagePGM nImg=new ImagePGM();
            nImg.IDimX = In.IDimX;
            nImg.IDimY = In.IDimY;
            nImg.Image[0] = In.Image[0];
            int IterationsCount=nImg.IDimX*nImg.IDimY;
            for (int i=1;i<IterationsCount;i++)            
                nImg.Image[i] -= nImg.Image[i - 1];            
            return nImg;
        }
    }
}
