﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{
    class BinarystreamFile
    {
        private FileStream input;
        private FileStream output;

        private byte buffer;
        private int bufferCount;

        public ulong counter = 0;

        public void OpenRead(String file)
        {
            CloseRead();

            input = new FileStream(file, FileMode.Open, FileAccess.Read);
            buffer = 0;
            bufferCount = 0;
        }

        public void OpenWrite(String file)
        {
            CloseWrite();

            output = new FileStream(file, FileMode.Create, FileAccess.Write);
            buffer = 0;
            bufferCount = 0;
            counter = 0;
        }

        public void CloseRead()
        {
            if(input != null)
            {
                input.Flush();
                input.Close();
            }

            input = null;
        }

        public void CloseWrite()
        {
            if (output != null)
            {
                output.Flush();
                output.Close();
            }

            output = null;
        }

        public void WriteBit(byte bit)
        {
            counter++;
            buffer <<= 1;
            bufferCount++;
            if (bit != 0)
                buffer |= 1;

            if(bufferCount == 8)
            {
                output.WriteByte(buffer);
                buffer = 0;
                bufferCount = 0;
            }
        }

        public void WriteNumberBits(ulong bits, int number)
        {
            byte write;
	        for (int i = 0; i < number; i++)
	        {
                write = 0;
                if ((bits & (ulong)((ulong)1 << (number - i - 1))) != 0)
                    write = 1;

		        WriteBit(write);
	        }
        }

        public void WriteCharacter(byte bits)
        {
            WriteNumberBits(bits, 8);
        }

        public void FinishWriting()
        {
	        if (bufferCount == 0)
		        return;

            WriteNumberBits(0, 8 - bufferCount);
        }

        public ushort ReadBit()
        {
	        if (bufferCount == 0)
	        {
		        buffer = (byte)input.ReadByte();
		        bufferCount = 8;
	        }

            ushort read = 0;
            if ((buffer & 0x80) != 0)
                read = 1;

	        buffer <<= 1;
	        bufferCount--;

	        return read;
        }

        public ulong ReadNumberBits(int number)
        {
	        ulong read = 0;

	        for (int i = 0; i < number; i++)
	        {
		        read <<= 1;
		        read |= ReadBit();
	        }

	        return read;
        }

        public byte ReadCharacter()
        {
	        return (byte)(ReadNumberBits(8));
        }
    }
}
