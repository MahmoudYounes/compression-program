﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{
    class tree
    {
        private List<Pair<double, node>> nodes;
        private node head;
        public tree(){}
        public void generate_all_tree()
        {            
            while (nodes.Count > 1)
            {
                nodes = nodes.OrderBy(o => o.First).ToList();
                string x = Convert.ToString(nodes[0].Second.Data.Second); //symbol of first node                
                x +=" "+ Convert.ToString(nodes[1].Second.Data.Second);       //added to symbol of second node
                node n = new node(Pair<double,string>.MakePair(nodes[0].First + nodes[1].First, x), nodes[0].Second, nodes[1].Second); //creating new node with and assigning its left and right nodes                
                nodes.Insert(nodes.Count,Pair<double,node>.MakePair(n.Data.First, n));
                nodes.RemoveAt(0);  //list is shifted again
                nodes.RemoveAt(0);  
            }
            head = nodes[0].Second;             
        }
        public void Copy(List<Pair<double,node>> vec)
        {
            nodes = vec;            
        }
        public void print_nodes(node n,int indent=0)
        {
            /*
            if (n == null)
                return;
            for (int i = 0; i < indent; i++)
                Console.Out.Write(' ');
            Console.Out.WriteLine(n.data.Item2 + ':' + n.data.Item1);            
            print_nodes(n.left, indent + 1);
            print_nodes(n.right, indent + 1);
              */
        }
        public node GetHead() { return head; }
    }
}
