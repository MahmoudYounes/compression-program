﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Multi
{
    class ImagePGM
    {
        public int IDimX;
        public int IDimY;
        int MaxColor;
        public List<int> Image;
        public string ImageName;
        public ImagePGM(string Name = "")
        {
            ImageName = Name;
            Image = new List<int>();
        }
        public bool ReadImg()
        {
            if (ImageName != "")
            {
                FileStreamInt stf = new FileStreamInt(ImageName, "r");
                stf.OpenFile();
                string temp = stf.LineRead();
                int x = stf.ReadInt(), y = stf.ReadInt(), _max = stf.ReadInt();
                IDimX = x;
                IDimY = y;
                MaxColor = _max;
                bool exit = false;
                while (!exit)
                {
                    int C = stf.ReadInt();
                    if (C == -1)
                        exit = true;
                    else
                        Image.Add(C);
                }
                stf.CloseFile();
                return true;
            }
            else return false;
        }
        public int this[int x, int y]
        {
            get { 
                return Image[(y * IDimX) + x]; 
            }
            set { Image[(y * IDimX) + x] = value; }
        }
    }
}
