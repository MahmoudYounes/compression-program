﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{
    class Pair<T1,T2>
    {
        public T1 First { get; set; }
        public T2 Second { get; set; }
        
        public Pair(T1 x,T2 y)
        {
            First = x;
            Second = y;
        }
        public Pair(){}
        public static Pair<T1,T2> MakePair(T1 in1,T2 in2)
        {
            return new Pair<T1, T2>(in1, in2);
        }
        public static bool operator <(Pair<T1,T2> i1,Pair<T1,T2> i2)
        {
            dynamic x = i1.First;
            dynamic y = i2.First;
            return x < y;
        }
        public static bool operator >(Pair<T1, T2> i1, Pair<T1, T2> i2)
        {
            dynamic x = i1.First;
            dynamic y = i2.First;
            return x > y;
        }
        public static bool operator <=(Pair<T1, T2> i1, Pair<T1, T2> i2)
        {
            dynamic x = i1.First;
            dynamic y = i2.First;
            return x <= y;
        }
        public static bool operator >=(Pair<T1, T2> i1, Pair<T1, T2> i2)
        {
            dynamic x = i1.First;
            dynamic y = i2.First;
            return x >= y;
        }
    }
}
