﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{
    class node
    {
        public node left;                       //ana bshawer 3ala l left
        public node right;                      //ana bshawer 3la l right

        public Pair<double, string> Data;       //probability And symbol
        public string Code;                     //assigned code for this node
        public node(Pair<double,string> d=null,node L = null, node R = null)
        {
            left = L;
            right = R;
            Data = d;
        }
       

    }
}
