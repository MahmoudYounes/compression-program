﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Multi
{
    class ArithmeticCode
    {
        private const int PRECISION = 32;
        private const int MAX_PROBABILITY = PRECISION - 2;
        private const uint Top_value = 0xFFFFFFFF;
        private const uint First_qtr = (uint)(Top_value / 4 + 1);
        private const uint Half = (uint)(2 * First_qtr);
        private const uint Third_qtr  = (uint)(3*First_qtr);

        private uint[] _probabilities = new uint[258];

        private BinarystreamFile _stream = new BinarystreamFile();
        private int _xDimension;
        private int _yDimension;
        private uint _totalProbability;
	    private uint _lower;
	    private uint _upper;
	    private uint _code;
	    private ulong _underflowBits;
	    private ulong _total;

        public ulong GetEncodedSize()
        {
            return _stream.counter;
        }
        private void GenerateProbabilities(string file)
        {
            Array.Clear(_probabilities, 0, _probabilities.Length);

            using (TextReader reader = File.OpenText(file))
            {
                _total = 0;
                _totalProbability = 0;
                string temp = reader.ReadToEnd();
                string[] input = Regex.Replace(temp, @"\s+", " ").Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (input[0] != "P2")
                    throw new Exception();

                _xDimension = int.Parse(input[1]);
                _yDimension = int.Parse(input[2]);

                //foreach (var item in input)
                for (int i = 4; i < input.Length; i++ )
                {
                    int x = int.Parse(input[i]);
                    _probabilities[x + 1]++;
                    _total++;
                }
            }

	        // _probabilities[k] = lower value of symbol k + 1 and higher value of symbol k
	        for (int i = 0; i < 256; i++)
	        {
		        _probabilities[i + 1] += _probabilities[i];
	        }	
	        _totalProbability = (uint)_probabilities[256];
        }

        public void EncodeInit(string outputFile, byte type)
        {
            _lower = 0;
            _upper = Top_value;
            _underflowBits = 0;
            _stream.OpenWrite(outputFile);
            _stream.WriteNumberBits(type, 2);
        }

        public void EncodeEnd()
        {
            WriteLeftBits();
            _stream.CloseWrite();
        }

        public void EncodeSymbol(AdaptiveModel model, int symbol)
        {
            ulong range;
            range = (ulong)(_upper - _lower) + 1;

            _upper = _lower;
            _upper += (uint)((range * (uint)model.GetHigh(symbol)) / (uint)model.GetCumulative() - 1);
            _lower += (uint)((range * (uint)model.GetLow(symbol)) / (uint)model.GetCumulative());

            Write();
            model.Update(symbol);
        }

        public void DecodeInit(string inputFile)
        {
            _stream.OpenRead(inputFile);
            _stream.ReadNumberBits(2);
            _lower = 0;
            _upper = Top_value;
            _code = (uint)_stream.ReadNumberBits((byte)PRECISION);
        }

        public int DecodeSymbol(AdaptiveModel model)
        {
            long range = (long)(_upper - _lower) + 1;

            long restoredProbability = (long)_code - _lower + 1;
            restoredProbability = (restoredProbability * model.GetCumulative() - 1) / range;

            int symbol = model.GetDecodedSymbol((int)restoredProbability);

            _upper = _lower;
            _upper += (uint)((range * (uint)model.GetHigh(symbol)) / (uint)model.GetCumulative() - 1);
            _lower += (uint)((range * (uint)model.GetLow(symbol)) / (uint)model.GetCumulative());

            Read();
            model.Update(symbol);

            return symbol;
        }

        public void DecodeEnd()
        {
            _stream.CloseRead();
        }

        public void Encode(string inputFile, string outputFile)
        {
	        GenerateProbabilities(inputFile);

	        ulong range;
	        uint value;

            _stream.OpenWrite(outputFile);
            _lower = 0;
            _upper = Top_value;
            _underflowBits = 0;

            WriteHeader();

            using (TextReader reader = File.OpenText(inputFile))
            {
                string temp = reader.ReadToEnd();
                string[] input = Regex.Replace(temp, @"\s+", " ").Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                //foreach (var item in input)
                for (int i = 4; i < input.Length; i++) 
                {
                    value = uint.Parse(input[i]);
                    // The 1 here is because the range is supposed to be [0, 1) and 1 here is equal 0.1111111111...b
                    // and since we are using the fraction part with the point therefor by adding 1 we reach 1.000 which is the total range
                    // at the beginning of encoding

                    range = (ulong)(_upper - _lower) + 1;

                    // first entry in the array is the higher of an undefined value but it's a lower to valid one
                    // so we add 1 when accessing the probability of "value"
                    // 1st entry is _probabilities[1] which corresponds symbol 0

                    _upper = _lower;
                    _upper += (uint)((range * _probabilities[value + 1]) / _totalProbability - 1);
                    _lower += (uint)((range * _probabilities[value]) / _totalProbability);

                    Write();
                }
            }
            WriteLeftBits();

            _stream.CloseWrite();
        }

        private void Write()
        {
	        while (true)
	        {
		        if (_upper < Half)
		        {
			        WriteWithUnderflow(0);
		        }
		        else if (_lower >= Half)
		        {
			        WriteWithUnderflow(1);

			        _lower -= Half;
			        _upper -= Half;
		        }
		        else if (_lower >= First_qtr && _upper < Third_qtr)
		        {
			        _underflowBits++;

			        _lower -= First_qtr;
			        _upper -= First_qtr;
		        }
		        else
		        {
			        break;
		        }

		        _lower <<= 1;
		        _upper <<= 1;
		        _upper |= 1;	// _upper is supposed to be an infinite number of ones so when we shift left we should add a 1
	        }
        }

        private void WriteWithUnderflow(uint bit)
        {
	        _stream.WriteBit((byte)bit);

	        while (_underflowBits > 0)
	        {
                if(bit == 0)
		            _stream.WriteBit(1);
                else
                    _stream.WriteBit(0);

		        _underflowBits--;
	        }
        }

        private void WriteLeftBits()
        {
	        _underflowBits++;
	        if (_lower < First_qtr)
	        {
		        WriteWithUnderflow(0);
	        }
	        else
	        {
		        WriteWithUnderflow(1);
	        }
	
	        _stream.FinishWriting();
        }

        public void Decode(string inputFile, string outputFile)
        {
            _stream.OpenRead(inputFile);
            ReadHeader();
            FileStream file = new FileStream(outputFile, FileMode.Create, FileAccess.Write);
            StreamWriter write = new StreamWriter(file);
            ulong total = _total;
            _lower = 0;
            _upper = Top_value;
            _code = (uint)_stream.ReadNumberBits((byte)PRECISION);
            ulong range;
            ulong restoredProbability;

            write.WriteLine("P2");
            write.WriteLine(_xDimension + " " + _yDimension);
            write.WriteLine("255");

	        while (total-- > 0)
	        {
                range = (ulong)(_upper - _lower) + 1;

                restoredProbability = (ulong)_code - _lower + 1;
                restoredProbability = (restoredProbability * _totalProbability - 1) / range;

                uint value = GetDecodedSymbol((uint)restoredProbability);

                char[] temp = (Convert.ToString(value) + " ").ToArray();
                write.Write(temp, 0, temp.Length);

                _upper = _lower;
                _upper += (uint)((range * _probabilities[value + 1]) / _totalProbability - 1);
                _lower += (uint)((range * _probabilities[value]) / _totalProbability);

                Read();
	        }

            write.Flush();
            write.Close();
            _stream.CloseRead();
        }

        private void Read()
        {
	        while (true)
	        {
		        if (_upper < Half)
		        {

		        }
		        else if (_lower >= Half)
		        {
			        _code -= Half;
			        _lower -= Half;
			        _upper -= Half;
		        }
		        else if (_lower >= First_qtr && _upper < Third_qtr)
		        {
			        _code -= First_qtr;
			        _lower -= First_qtr;
			        _upper -= First_qtr;
		        }
		        else
		        {
			        break;
		        }

		        _lower <<= 1;
		        _upper <<= 1;
		        _upper |= 1;	// _upper is supposed to be an infinite number of ones so when we shift left we should add a 1
		        _code <<= 1;
		        _code |= _stream.ReadBit();
	        }
        }

        private uint GetDecodedSymbol(uint probability)
        {
            uint first = 0;
            uint last = 260;
	        uint medium;

	        while (first <= last)
	        {
		        medium = (uint)((first + last) / 2);

		        if (_probabilities[medium] > probability)
		        {
			        last = (uint)(medium - 1);
		        }
		        else if (_probabilities[medium + 1] <= probability)
		        {
			        first = (uint)(medium + 1);
		        }
		        else
		        {
			        return medium;
		        }
	        }

            throw new Exception();
        }

        private void WriteHeader()
        {
            _stream.WriteNumberBits(1, 2);
            _stream.WriteCharacter((byte)(_xDimension - 1));
            _stream.WriteCharacter((byte)(_yDimension - 1));
	        for (int i = 1; i <= 256; i++)
	        {
		        if (_probabilities[i] != _probabilities[i - 1])
		        {
                    _stream.WriteBit(1);
                    if (_probabilities[i] - _probabilities[i - 1] > 256)
                    {
                        _stream.WriteBit(1);
                        _stream.WriteNumberBits((ulong)(_probabilities[i] - _probabilities[i - 1] - 1), 16);
                    }
                    else
                    {
                        _stream.WriteBit(0);
                        _stream.WriteNumberBits((ulong)(_probabilities[i] - _probabilities[i - 1] - 1), 8);
                    }
		        }
                else
                {
                    _stream.WriteBit(0);
                }
	        }
        }

        private void ReadHeader()
        {
            _stream.ReadNumberBits(2);
            Array.Clear(_probabilities, 0, _probabilities.Length);
            _xDimension = (int)_stream.ReadCharacter() + 1;
            _yDimension = (int)_stream.ReadCharacter() + 1;
            _total = (ulong)(_xDimension * _yDimension);
	        ushort input;
            uint prob;
	        ulong temp = _total;

            for (int i = 0; i < 256; i++) 
            {
                input = _stream.ReadBit();

                if (input == 0)
                    continue;

                input = _stream.ReadBit();
                if(input == 0)
                {
                    prob = (uint)_stream.ReadCharacter();
                }
                else
                {
                    prob = (uint)_stream.ReadNumberBits(16);
                }

                _probabilities[i + 1] = prob + 1;
            }
	
	        for (int i = 0; i <= 256; i++)
	        {
		        _probabilities[i + 1] += _probabilities[i];
	        }
	        _totalProbability = (uint)_probabilities[256];
        }
    }
}
