﻿//using ArithmeticCoding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;



namespace Multi
{


    class CALIC
    {

        ImagePGM Image;                 //image input to CALIC
        public const int DefaultPixelValue = 0;         //default pixel value: value for pixels outside the frame of image
        public const int NumberOfSymbols = 256;

        private List<int> PredictedAGSPError;       //saving errors
        private ImagePGM PredictedGAP;              //saving prediction

        private List<Pair<int, int>> ErrorFreq;     //to calculate mean error
        private List<int> Errors;

        private int[,] DifForDirs = { { -1, 0 }, { -2, 0 }, { 1, -1 }, { 0, -1 }, { -1, -1 }, { -2, -1 }, { 1, -2 }, { 0, -2 }, { -1, -2 } };
        private int[] HistogramTailTruncation = { 18, 26, 34, 50, 66, 82, 114, 256 };

        //[0]->W,[1]->WW,[2]->NE,[3]->N,[4]->NW,[5]->NWW,[6]->NNE,[7]->NN,[8]->NNW
        private enum Dir { W = 0, WW = 1, NE = 2, N = 3, NW = 4, NWW = 5, NNE = 6, NN = 7, NNW = 8 };

        private int[,] CausalDif = { { -1, 0 }, { 0, -1 }, { 1, -1 }, { -1, -1 } };

        private int[] Quantizers = { 5, 15, 25, 42, 60, 85, 140 };

        ArithmeticCode coder;
        ImagePGM Decompressed;
        ImagePGM RetrievedErrors;

        AdaptiveModel[] Models;

        public CALIC(ImagePGM img = null)
        {
            Image = img;
            PredictedAGSPError = new List<int>();
            PredictedGAP = new ImagePGM();
            ErrorFreq = new List<Pair<int, int>>();
            Errors = new List<int>();
            Decompressed = new ImagePGM();
            RetrievedErrors = new ImagePGM();
            Models = new AdaptiveModel[8];
            coder = new ArithmeticCode();
            for (int i = 0; i < 8; i++ )
            {
                Models[i] = new AdaptiveModel();
            }
            for (int i = 0; i < 2048; i++)
                ErrorFreq.Add(Pair<int, int>.MakePair(0, 0));

        }

        private int GetPixelValue(int x, int y, int xDifference = 0, int yDifference = 0)
        {
            if (x - xDifference < 0 || y - yDifference < 0)
                return DefaultPixelValue;
            else return Image[(x - xDifference), (y - yDifference)];
        }

        private int GetPredictedValue(int x, int y, int xDifference = 0, int yDifference = 0)
        {
            if (x - xDifference < 0 || y - yDifference < 0)
                return DefaultPixelValue;
            return PredictedGAP[(x - xDifference), (y - yDifference)];
        }
        private bool SetPixelValue(int x, int y, int v)
        {
            if (x < 0 || y < 0)
                return false;
            Decompressed[x, y] = v;
            return true;
        }

        /* Inputs: pixel index */
        private List<int> GetPixelNeighbours(int x, int y)
        {
            //[0]->W,[1]->WW,[2]->NE,[3]->N,[4]->NW,[5]->NWW,[6]->NNE,[7]->NN,[8]->NNW
            List<int> n = new List<int>(9);


            for (int i = 0; i < DifForDirs.GetLength(0); i++)
                n.Add(GetPixelValue(x + DifForDirs[i, 0], y + DifForDirs[i, 1]));
            return n;
        }

        private Tuple<int, int> PredictPixelValuesGAP(int x, int y)
        {
            List<int> PN = GetPixelNeighbours(x, y);

            int Dh =
                Math.Abs(PN[(int)Dir.W] - PN[(int)Dir.WW]) +
                Math.Abs(PN[(int)Dir.N] - PN[(int)Dir.NW]) +
                Math.Abs(PN[(int)Dir.NE] - PN[(int)Dir.N]);
            int Dv =
                Math.Abs(PN[(int)Dir.W] - PN[(int)Dir.NW]) +
                Math.Abs(PN[(int)Dir.N] - PN[(int)Dir.NN]) +
                Math.Abs(PN[(int)Dir.NE] - PN[(int)Dir.NNE]);

            int xHat = -1;
            if (Dv - Dh > 80)
                xHat = PN[(int)Dir.W];
            else if (Dv - Dh < -80)
                xHat = PN[(int)Dir.N];
            else
            {
                int N = PN[(int)Dir.N], W = PN[(int)Dir.W], NE = PN[(int)Dir.NE], NW = PN[(int)Dir.NW];
                xHat = (N + W);
                xHat = xHat >> 1;
                xHat += ((NE - NW) / 4);

                if (Dv - Dh > 32)
                    xHat = (xHat + W) >> 1;
                else if (Dv - Dh > 8)
                    xHat = (3 * xHat + W) >> 2;
                else if (Dv - Dh < -32)
                    xHat = (xHat + N) >> 1;
                else if (Dv - Dh < -8)
                    xHat = (3 * xHat + N) >> 2;
            }

            int eWest = GetPixelValue(x - 1, y) - GetPredictedValue(x - 1, y);
            int eNorth = GetPixelValue(x, y - 1) - GetPredictedValue(x, y - 1);


            int delta = Dh + Dv + Math.Abs(eWest) + Math.Abs(eNorth);
            return new Tuple<int, int>(xHat, delta);
        }

        private int GetContextInfo(int xDot, int Delta, int x, int y)
        {
            int B = 0, N = GetPixelValue(x, y - 1), W = GetPixelValue(x - 1, y), NN = GetPixelValue(x, y - 2), WW = GetPixelValue(x - 2, y);
            if (xDot > N)   //N
                B |= 1;

            if (xDot > W)   //W
                B |= 2;

            if (xDot > GetPixelValue(x - 1, y - 1)) //  NW
                B |= 4;

            if (xDot > GetPixelValue(x + 1, y - 1)) //   NE
                B |= 8;

            if (xDot > NN)  //  NN
                B |= 16;

            if (xDot > WW) //  WW
                B |= 32;

            if (xDot > 2 * N - NN)  // 2*N-NN
                B |= 64;

            if (xDot > 2 * W - WW) // 2*W-WW
                B |= 128;

            Delta = Delta >> 1;
            int ContextInfo = (Delta & 3) | (B << 2);
            return ContextInfo;
        }

        private int QuantizeDelta(int Delta)
        {
            int i = 0;
            for (; i < Quantizers.Length; i++)
            {
                if (Delta < Quantizers[i])
                    return i;
            }
            return 7;
        }

        private bool DumpDecompressedImage(string outputFile)
        {
            FileStreamInt f = new FileStreamInt(outputFile, "w");
            try
            {
                f.OpenFile();
                f.Write("P2\n");
                f.WriteInt(Convert.ToString(Decompressed.IDimX));
                f.Write(" ");
                f.WriteInt(Convert.ToString(Decompressed.IDimY));
                f.Write("\n");
                f.WriteInt("255");
                f.Write("\n");
                //int j = 1;
                for (int i = 0; i < Decompressed.Image.Count; i++)
                {
                    if (i % 20 == 0 && i != 0)
                        f.Write("\n");

                    f.WriteInt(Convert.ToString(Decompressed.Image[i]));
                    f.Write(" ");
                }
                f.CloseFile();
                return true;
            }
            catch
            {

                return false;
            }
        }

        private void InitializeDecompressedImage()
        {
            for (int i = 0; i < Decompressed.IDimX; i++)
                for (int j = 0; j < Decompressed.IDimY; j++)
                    Decompressed.Image.Add(0);
        }

        private void ReInitializeErrorFreq()
        {
            for (int i = 0; i < ErrorFreq.Count; i++)
                ErrorFreq[i].First = ErrorFreq[i].Second = 0;
        }

        public void Encode(string outputFile)
        {
            coder.EncodeInit(outputFile, 0);
            PredictedGAP.IDimX = Image.IDimX;
            PredictedGAP.IDimY = Image.IDimY;
            AdaptiveModel temp = new AdaptiveModel();
            coder.EncodeSymbol(temp, Image.IDimX - 1);
            coder.EncodeSymbol(temp, Image.IDimY - 1);

            for (int j = 0; j < Image.IDimY; j++)
            {
                for (int i = 0; i < Image.IDimX; i++)
                {
                    Tuple<int, int> ContextData = PredictPixelValuesGAP(i, j);       //prediction step & calculating delta
                    int IPredicted = ContextData.Item1;
                    int delta = ContextData.Item2;

                    delta = QuantizeDelta(delta);
                    int COfDB = GetContextInfo(IPredicted, delta, i, j), ExpectedError = 0;  // C(delta,Beta)

                    if (ErrorFreq[COfDB].Second != 0)
                        ExpectedError = ErrorFreq[COfDB].First / ErrorFreq[COfDB].Second;

                    /*/////////////////////////////////////////////////////////////////////*/
                    /*/////////////////////////////////////////////////////////////////////*/
                    IPredicted += ExpectedError;                //make sure that the predicted pixel is in range (0, 255)
                    if (IPredicted < 0)
                        IPredicted = 0;
                    else if (IPredicted > 255)
                        IPredicted = 255;

                    PredictedGAP.Image.Add(IPredicted);        //saving predicted value
                    /*/////////////////////////////////////////////////////////////////////*/
                    /*/////////////////////////////////////////////////////////////////////*/

                    int NewError = GetPixelValue(i, j) - IPredicted;

                    ErrorFreq[COfDB].Second++;
                    ErrorFreq[COfDB].First += NewError;

                    if (ErrorFreq[COfDB].Second < 0)
                        NewError = -NewError;

                    AddError(delta, EncodeRemapError(IPredicted, NewError));

                    if (ErrorFreq[COfDB].Second >= 128)
                    {
                        ErrorFreq[COfDB].First = ErrorFreq[COfDB].First / 2;
                        ErrorFreq[COfDB].Second = ErrorFreq[COfDB].Second / 2;
                    }
                }
            }

            //EncodeErrors();
            coder.EncodeEnd();
        }

        public void AddError(int delta, int error)
        {
            int escaped = HistogramTailTruncation[delta] - 1;

            if (error < escaped)
                coder.EncodeSymbol(Models[delta], error);
            else
            {
                coder.EncodeSymbol(Models[delta], escaped);

                if (delta + 1 < HistogramTailTruncation.Length)
                    AddError(delta + 1, error - escaped);
            }
        }

        public int ReadError(int delta)
        {
            int error = coder.DecodeSymbol(Models[delta]);
            int escaped = HistogramTailTruncation[delta] - 1;

            if (error != escaped || (delta + 1) == HistogramTailTruncation.Length)
                return error;

            return escaped + ReadError(delta + 1);
        }

        public void Decode(string inputFile, string outputFile)
        {
            coder.DecodeInit(inputFile);
            AdaptiveModel temp = new AdaptiveModel();

            Image = Decompressed;
            Decompressed.IDimX = PredictedGAP.IDimX = coder.DecodeSymbol(temp) + 1;
            Decompressed.IDimY = PredictedGAP.IDimY = coder.DecodeSymbol(temp) + 1;

            InitializeDecompressedImage();
            ReInitializeErrorFreq();
            for (int j = 0; j < Decompressed.IDimY; j++)
            {
                for (int i = 0; i < Decompressed.IDimX; i++)
                {
                    Tuple<int, int> ContextData = PredictPixelValuesGAP(i, j);       //prediction step & calculating delta
                    int IPredicted = ContextData.Item1;
                    int delta = ContextData.Item2;

                    delta = QuantizeDelta(delta);
                    int COfDB = GetContextInfo(IPredicted, delta, i, j), ExpectedError = 0;  // C(delta,Beta)

                    if (ErrorFreq[COfDB].Second != 0)
                        ExpectedError = ErrorFreq[COfDB].First / ErrorFreq[COfDB].Second;

                    /*/////////////////////////////////////////////////////////////////////*/
                    /*/////////////////////////////////////////////////////////////////////*/
                    IPredicted += ExpectedError;                //make sure that the predicted pixel is in range (0, 255)                    
                    if (IPredicted < 0)
                        IPredicted = 0;
                    else if (IPredicted > 255)
                        IPredicted = 255;

                    PredictedGAP.Image.Add(IPredicted);        //saving predicted value
                    /*/////////////////////////////////////////////////////////////////////*/
                    /*/////////////////////////////////////////////////////////////////////*/

                    int NewError = DecodeRemapError(IPredicted, ReadError(delta));   //implement GetErrorValue

                    if (ErrorFreq[COfDB].Second < 0)
                        NewError = -NewError;

                    int restoredPixel = IPredicted + NewError;

                    SetPixelValue(i, j, restoredPixel);                              //implement this too

                    ErrorFreq[COfDB].Second++;
                    ErrorFreq[COfDB].First += NewError;

                    if (ErrorFreq[COfDB].Second >= 128)
                    {
                        ErrorFreq[COfDB].First = ErrorFreq[COfDB].First / 2;
                        ErrorFreq[COfDB].Second = ErrorFreq[COfDB].Second / 2;
                    }
                }
            }
            coder.DecodeEnd();
            DumpDecompressedImage(outputFile);
        }

        public string e { get; set; }

        private int EncodeRemapError(int predicted, int error)
        {
            int halfSymbols = (NumberOfSymbols >> 1) - 1;
            int absoluteError = Math.Abs(error);

            if (error == 0)
                return error;

            if (predicted < halfSymbols)
            {
                if (absoluteError <= predicted)
                {
                    if (error > 0)
                    {
                        error = 2 * absoluteError - 1;
                    }
                    else
                    {
                        error = 2 * absoluteError;
                    }
                }
                else
                {
                    error += predicted;
                }
            }
            else
            {
                if (absoluteError <= NumberOfSymbols - predicted - 1)
                {
                    if (error > 0)
                    {
                        error = 2 * absoluteError - 1;
                    }
                    else
                    {
                        error = 2 * absoluteError;
                    }
                }
                else
                {
                    error = NumberOfSymbols - predicted - 1 + absoluteError;
                }
            }

            return error;
        }

        private int DecodeRemapError(int predicted, int error)
        {
            int halfSymbols = (NumberOfSymbols >> 1) - 1;

            if (error == 0)
                return error;

            if (predicted < halfSymbols)
            {
                if (error <= 2 * predicted)
                {
                    if (error % 2 == 0)
                    {
                        error = -(error >> 1);
                    }
                    else
                    {
                        error = (error >> 1) + 1;
                    }
                }
                else
                {
                    error = error - predicted;
                }
            }
            else
            {
                if (error <= 2 * (NumberOfSymbols - predicted - 1))
                {
                    if (error % 2 == 0)
                    {
                        error = -(error >> 1);
                    }
                    else
                    {
                        error = (error >> 1) + 1;
                    }
                }
                else
                {
                    error = NumberOfSymbols - (error + predicted + 1);
                }
            }

            return error;
        }

        public ulong GetEncodedSize()
        {
            return coder.GetEncodedSize();
        }
    }
}
