﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Multi
{
    class Statistics
    {
        private double[] GetProbabilities(string file)
        {
            double[] probabilities = new double[256];
            Array.Clear(probabilities, 0, probabilities.Length);
            int total = 0;

            using (TextReader reader = File.OpenText(file))
            {
                string temp = reader.ReadToEnd();
                string[] input = Regex.Replace(temp, @"\s+", " ").Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (input[0] != "P2" || int.Parse(input[1]) == 0 || int.Parse(input[2]) == 0)
                    throw new Exception();

                for(int i = 4; i < input.Length; i++)
                {
                    probabilities[int.Parse(input[i])]++;
                    total++;
                }

                for(int i =0; i < 256; i++)
                {
                    probabilities[i] /= total;
                }
            }

            return probabilities;
        }

        public double GetEntropy(string file)
        {
            double[] probabilities = GetProbabilities(file);
            double entropy = 0;

            for(int i = 0; i < 256; i++)
            {
                if(probabilities[i] != 0)
                    entropy += probabilities[i] * Math.Log(1 / probabilities[i], 2);
            }

            return entropy;
        }

        public double GetCompressionRatio(string file, int compressedSize)
        {
            return GetSize(file) / (double)compressedSize;
        }

        public long GetSize(string file)
        {
            return new FileInfo(file).Length;
        }

        public void PrintStatistics(string file, int compressedSize)
        {
            using (FileStream fileStream = new FileStream("Statistics.txt", FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter write = new StreamWriter(fileStream))
                {
                    write.WriteLine("Entropy: " + GetEntropy(file));
                    write.WriteLine();
                    write.WriteLine("Compression Ratio: " + GetCompressionRatio(file, compressedSize));
                }
            }
        }
    }
}
