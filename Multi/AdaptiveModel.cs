﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{

    public class AdaptiveModel
    {
        private int[] Frequencies;
        private int[] Cumulative;
        private int[] SymbolToIndex;
        private int[] IndexToSymbol;

        public AdaptiveModel()
        {
            Frequencies = new int[257];
            Cumulative = new int[257];
            SymbolToIndex = new int[257];
            IndexToSymbol = new int[257];

            Frequencies[0] = 0;
            for (int i = 1; i <= 256; i++)
                Frequencies[i] = 1;

            for(int i = 256, total = 0; i >= 0; i--)
            {
                Cumulative[i] = total;
                total += Frequencies[i];
            }

            for (int i = 0; i < 256; i++)
            {
                SymbolToIndex[i] = i + 1;
                IndexToSymbol[i + 1] = i;
            }
        }

        public int GetHigh(int symbol)       //hueueueueueueueue
        {
            int index = SymbolToIndex[symbol];
            return Cumulative[index - 1];
        }

        public int GetLow(int symbol)
        {
            int index = SymbolToIndex[symbol];
            return Cumulative[index];
        }

        public int GetCumulative()
        {
            return Cumulative[0];
        }

        public void Update(int symbol)
        {
            int index = SymbolToIndex[symbol];
            int i, symbolOfI, symbolOfIndex;
            for (i = index; Frequencies[i] == Frequencies[i - 1]; i--) ;

            if(i < index)
            {
                symbolOfI = IndexToSymbol[i];
                symbolOfIndex = IndexToSymbol[index];
                IndexToSymbol[i] = symbolOfIndex;
                IndexToSymbol[index] = symbolOfI;
                SymbolToIndex[symbolOfI] = index;
                SymbolToIndex[symbolOfIndex] = i; 
            }

            Frequencies[i]++;

            while(i > 0)
            {
                Cumulative[--i]++;
            }
        }

        public int GetDecodedSymbol(int restoredProbability)
        {
            int index;
            for (index = 1; Cumulative[index] > restoredProbability; index++) ;

            return IndexToSymbol[index];
        }
    }
}
