﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args[0] == "-encode")
            {
                Statistics stats = new Statistics();

                ImagePGM img = new ImagePGM(args[1]);
                img.ReadImg();

                CALIC CompSample = new CALIC(img);
                CompSample.Encode("Calic.bin");
                double CALICSize = Math.Ceiling(CompSample.GetEncodedSize() / 8.0);
                Console.WriteLine(CALICSize);

                ArithmeticCode t = new ArithmeticCode();
                t.Encode(args[1], "Arth.bin");
                double ArithSize = Math.Ceiling(t.GetEncodedSize() / 8.0);
                Console.WriteLine(ArithSize);

                Huffman h = new Huffman(img);
                h.Encode("Huffman.bin");
                double HuffSize = Math.Ceiling(h.GetEncodedSize() / 8.0);
                Console.WriteLine(HuffSize);

                if (CALICSize < ArithSize && CALICSize < HuffSize)
                {
                    stats.PrintStatistics(args[1], (int)CALICSize);
                    File.Delete("Arth.bin");
//                    File.Delete("Huffman.bin");
                    File.Delete(args[2]);
                    File.Move("Calic.bin", args[2]);
                }
                else if (ArithSize < CALICSize && ArithSize < HuffSize)
                {
                    stats.PrintStatistics(args[1], (int)ArithSize);
                    File.Delete("Calic.bin");
                    File.Delete("Huffman.bin");
                    File.Delete(args[2]);
                    File.Move("Arth.bin", args[2]);
                }
                else
                {
                    stats.PrintStatistics(args[1], (int)HuffSize);
                    File.Delete("Calic.bin");
                    File.Delete("Arth.bin");
                    File.Delete(args[2]);
                    File.Move("Huffman.bin", args[2]);
                }
            }
            else if (args[0] == "-decode")
            {
                BinarystreamFile binary = new BinarystreamFile();
                binary.OpenRead(args[1]);
                ulong type = binary.ReadNumberBits(2);
                binary.CloseRead();
                if (type == 0)
                {
                    CALIC CompSample = new CALIC();
                    CompSample.Decode(args[1], args[2]);
                }
                else if (type == 1)
                {
                    ArithmeticCode t = new ArithmeticCode();
                    t.Decode(args[1], args[2]);
                }
                else if (type==2)
                {
                    Huffman h = new Huffman();
                    h.Decode(args[1], args[2]);
                }
            }
        }
    }
}
