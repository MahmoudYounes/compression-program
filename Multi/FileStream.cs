﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Multi
{
    class FileStreamInt
    {
        string FileName, access;
        StreamReader sr;
        StreamWriter sw;
        List<string> numbers;
        public FileStreamInt(string NameIn = "", string Iaccess = "")
        {
            FileName = NameIn;
            access = Iaccess;
            numbers = new List<string>();
        }

        public bool OpenFile()
        {
            try
            {
                if (access == "r")
                    sr = new StreamReader(FileName);
                else if (access == "w")
                    sw = new StreamWriter(FileName);
                else if (access == "rw")
                {
                    sr = new StreamReader(FileName);
                    sw = new StreamWriter(FileName);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
                return false;
            }
        }
        public bool CloseFile()
        {
            try
            {
                if (sw != null)
                    sw.Close();
                if (sr != null)
                    sr.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
                return false;
            }
        }

        public int ReadInt()
        {
            if (access != "r" && access != "rw")
                return -1;
            while (numbers.Count == 0)       // have we returned the whole current line read or no?
            {
                string Line;
                if ((Line = sr.ReadLine()) == null)
                    return -1;
                numbers = Regex.Replace(Line, @"\s+", " ").Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            string x = numbers[0];
            numbers.Remove(x);
            return int.Parse(x);
        }

        public string LineRead()
        {
            if (access != "r" && access != "rw")
                return "";
            return sr.ReadLine();
        }

        public bool LineWrite(string Line)
        {
            if (access != "w" && access != "rw")
                return false;
            sw.WriteLine(Line);
            return true;
        }
        public bool WriteInt(string Line)
        {
            if (access != "w" && access != "rw")
                return false;
            sw.Write(Convert.ToString(Line));
            return true;
        }

        public bool Write(string Line)
        {
            if (access != "w" && access != "rw")
                return false;
            sw.Write(Line);
            return true;
        }
    }
}
