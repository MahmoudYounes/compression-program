﻿using Multi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi
{
    class Huffman
    {
        ImagePGM Image;
        private const int DPV = 0;
        //for encoding
        Dictionary<string, double> Probs;
        Dictionary<int, int> ProbsSorted;
        Dictionary<string, string> SymbolCodes;
        tree huff;
        ulong EncodingSize;
        BinarystreamFile fBinOut;

        //for decoding
        Dictionary<string, double> ProbsIn;
        tree Decodedhuff;
        int DimX,DimY;
        Dictionary<string, string> SymbolDeCodes;
        ImagePGM DecodedImage;

        double entropy;
        double AverageCodewrodLength;
        int MaxCodewordLength, SymbolCount;


        public Huffman(ImagePGM img=null)
        {
            Image = img;
            huff = new tree();
            Probs = new Dictionary<string, double>();
            ProbsIn = new Dictionary<string, double>();
            SymbolDeCodes=new Dictionary<string,string>();
            Decodedhuff=new tree();
            ProbsSorted = new Dictionary<int, int>();
            SymbolCodes = new Dictionary<string, string>();
            DecodedImage=new ImagePGM();
            fBinOut = new BinarystreamFile();
            EncodingSize = 0;
            for (int i = 0; i < 256; i++)	 //initializing map	
            {
                string t = Convert.ToString(i);
                Probs.Add(t, 0);
            }
            SymbolCount = 0;
            if (img != null)
            {
                for (int i = 0; i < Image.Image.Count; i++)
                    SymbolCount++;
            }
            MaxCodewordLength = 0;
        }
        private int GetPixelValue(int x, int y, int x_dif = 0, int y_dif = 0)
        {
            if (x - x_dif < 0 || y - y_dif < 0)
                return DPV;
            else return Image[(x - x_dif), (y - y_dif)];
        }
        private void GetReleativeFreq()
        {
            for (int i = 0; i < Image.IDimX; i++)
                for (int j = 0; j < Image.IDimY; j++)
                {
                    Probs[Convert.ToString(GetPixelValue(i, j))]++;
                }
            SymbolCount = Image.IDimX * Image.IDimY;
        }
        private void GetEntropy()
        {

            foreach (KeyValuePair<string, double> it in Probs)
            {
                double p = Convert.ToDouble(it.Value) / SymbolCount;
                if (p != 0)
                {
                    double logP = Math.Log(p) / Math.Log(2);
                    entropy -= p * logP;
                }
            }
        }
        private void ConstructHuffmanTree(int number)
        {
            if (number == 0)
            {
                ///////////////////////////

                //////////////////////////
                List<node> vec = new List<node>();
                foreach (KeyValuePair<string, double> it in Probs) //preparing all nodes 
                {
                    if (it.Value > 0)
                    {
                        node n = new node(Pair<double, string>.MakePair(it.Value, it.Key));
                        vec.Add(n);
                    }
                }
                List<Pair<double, node>> vec2 = new List<Pair<double, node>>(); //Double => probabilities , & node
                for (int i = 0; i < vec.Count; i++)
                    vec2.Add(Pair<double, node>.MakePair(vec[i].Data.First, vec[i]));

                vec2 = vec2.OrderBy(o => o.First).ToList();
                huff.Copy(vec2);
                huff.generate_all_tree();
            }
            else if (number == 1)
            {
                List<node> vec = new List<node>();
                foreach (KeyValuePair<string, double> it in ProbsIn)
                {
                    if (it.Value > 0)
                    {
                        node n = new node(Pair<double, string>.MakePair(it.Value, it.Key));
                        vec.Add(n);
                    }
                }
                List<Pair<double, node>> vec2 = new List<Pair<double, node>>();
                for (int i = 0; i < vec.Count; i++)
                    vec2.Add(Pair<double, node>.MakePair(vec[i].Data.First, vec[i]));

                vec2 = vec2.OrderBy(o => o.First).ToList();
                Decodedhuff.Copy(vec2);
                Decodedhuff.generate_all_tree();
            }
        }
        private void LoopThroughTreeAndAssignCodes(node it)
        {
            if (it.left == null || it.right == null)
                return;
            it.left.Code += it.Code + "0";
            it.right.Code += it.Code + "1";
            LoopThroughTreeAndAssignCodes(it.left);
            LoopThroughTreeAndAssignCodes(it.right);
        }
        private void GetAllSymbolsCode(node it)
        {
            if (it.left == null || it.right == null)
            {
                SymbolCodes[it.Data.Second] = it.Code;
                return;
            }
            GetAllSymbolsCode(it.left);
            GetAllSymbolsCode(it.right);
        }
        private void GetAllSymbolsDeCode(node it)
        {
            if (it.left == null || it.right == null)
            {
                SymbolDeCodes[it.Code] = it.Data.Second;
                return;
            }
            GetAllSymbolsDeCode(it.left);
            GetAllSymbolsDeCode(it.right);
        }
        private void AssignHuffmanCode(tree Ihuff,int number)
        {
            LoopThroughTreeAndAssignCodes(Ihuff.GetHead());
            if (number == 0)
                GetAllSymbolsCode(Ihuff.GetHead());
            else if (number == 1)
                GetAllSymbolsDeCode(Ihuff.GetHead());
        }
        private void OutputBinaryFile(string fname)
        {
            BinarystreamFile fbin = fBinOut;
            FileStreamInt f = new FileStreamInt("Dictionary.txt", "w");
            try
            {
                fbin.OpenWrite(fname);
                //write file type
                fbin.WriteBit(1);
                fbin.WriteBit(0);
                EncodingSize += 2;
                //outputting Dimensions
                fbin.WriteNumberBits((ulong)(Image.IDimX-1), 8);                
                fbin.WriteNumberBits((ulong)(Image.IDimY-1), 8);
                EncodingSize += 16;     //2 bytes for dimensions
                //outputting header (Tree)
                foreach (KeyValuePair<string, double> it in Probs)
                {
                    if (it.Value == 0)
                    {
                        fbin.WriteBit(0);
                        EncodingSize++;
                    }
                    else
                    {
                        fbin.WriteBit(1);
                        EncodingSize++;
                        if (it.Value - 1 > 255)
                        {
                            fbin.WriteBit(1);                            
                            fbin.WriteNumberBits((ulong)it.Value, 16);
                            EncodingSize += 17; // 1 bit signifying 2 bytes written .. 16 bits => 2 bytes written
                        }
                        else
                        {
                            fbin.WriteBit(0);
                            fbin.WriteNumberBits((ulong)it.Value, 8);
                            EncodingSize += 9;
                        }
                    }
                }
                //outputting message
                foreach (int it in Image.Image)
                {
                    string CodeAsString = SymbolCodes[Convert.ToString(it)];                    
                    int size = CodeAsString.Length-1;
                    foreach (char c in CodeAsString)
                    {
                        if (c == '1')
                        {
                            fbin.WriteBit(1);
                        }
                        else if(c=='0')
                        {
                            fbin.WriteBit(0);
                        }
                    }                    
                }
                fbin.FinishWriting();
                fbin.CloseWrite();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }
        }
        private BinarystreamFile InputHeaderFromFile(string fname)
        {
            BinarystreamFile fbin = new BinarystreamFile();
            ulong bitsRead=0;
            try
            {
                fbin.OpenRead(fname);
                fbin.ReadNumberBits(2);
                //Reading SymbolCount
                DimX=Convert.ToInt32(fbin.ReadNumberBits(8))+1;
                DimY=Convert.ToInt32(fbin.ReadNumberBits(8))+1;                
                //Reading Header
                for (int i = 0; i < 256; i++)
                {
                    ulong b0 = fbin.ReadBit();
                    bitsRead++;
                    if (b0 == 0)
                        ProbsIn.Add(Convert.ToString(i), 0);
                    else if (b0 == 1)
                    {
                        ulong b1 = fbin.ReadBit();
                        ulong number = 0;
                        if (b1 == 0)
                        {
                            number = fbin.ReadNumberBits(8);
                            bitsRead+=8;
                        }
                        else if (b1 == 1)
                        {
                            number = fbin.ReadNumberBits(16);
                            bitsRead+=16;
                        }
                        ProbsIn.Add(Convert.ToString(i), number);
                    }
                }               
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }
            return fbin;
        }
        private void DecodeMessage(BinarystreamFile fbin)
        {
            int LoopCount = DimX * DimY,i=0;        //exit loop only when all Symbols Decoded
            string x = "";
            DecodedImage.IDimX = DimX;
            DecodedImage.IDimY = DimY;
            while(i<LoopCount)
            {
                x += Convert.ToString(fbin.ReadBit());
                if(SymbolDeCodes.ContainsKey(x))
                {
                    DecodedImage.Image.Add(Convert.ToInt32(SymbolDeCodes[x]));
                    x = "";
                    i++;
                }
            }
            
            fbin.CloseRead();
        }
        private void DumpDecodedImage(string fout)
        {
            FileStreamInt f = new FileStreamInt(fout, "w");
            try
            {
                f.OpenFile();
                f.LineWrite("P2");
                f.LineWrite(Convert.ToString(DecodedImage.IDimX) + " " + Convert.ToString(DecodedImage.IDimY));
                f.LineWrite("255");
                int j = 1;
                foreach (int it in DecodedImage.Image)
                {
                    f.Write(Convert.ToString(it));
                    if (j % 39 == 0)
                        f.Write("\n");
                    else
                        f.Write("\t");
                    j++;
                }
                f.CloseFile();
            }
            catch(Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }
        }
        public void Encode(string fname = "ImageHuffman.bin")
        {
            GetReleativeFreq();
            GetEntropy();
            ConstructHuffmanTree(0);
            AssignHuffmanCode(huff,0);
            OutputBinaryFile(fname);
        }
        public void Decode(string fname = "ImageHuffman.bin",string fout="DecodedImg.pgm")
        {
            BinarystreamFile fbin=InputHeaderFromFile(fname);
            ConstructHuffmanTree(1);
            AssignHuffmanCode(Decodedhuff,1);
            DecodeMessage(fbin);
            DumpDecodedImage(fout);
        }
        public void DumpStatistics()
        {
            FileStreamInt f = new FileStreamInt("MetaData.txt", "w");
            if (!f.OpenFile())
                Console.Out.WriteLine("File did not Open :'(.");
            foreach (KeyValuePair<string, double> it in Probs)
                f.LineWrite(it.Key + ": " + it.Value);
            f.LineWrite("entropy=" + entropy);
            f.CloseFile();
        }
        public ulong GetEncodedSize()
        {
            return fBinOut.counter;
        }

    }
}
